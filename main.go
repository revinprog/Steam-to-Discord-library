package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

type GameList struct {
	Applist struct {
		Apps []struct {
			Appid int    `json:"appid"`
			Name  string `json:"name"`
		} `json:"apps"`
	} `json:"applist"`
}

type Game struct {
	Success bool `json:"success"`
	Data    struct {
		Name                string      `json:"name"`
		IsFree              bool        `json:"is_free"`
		DetailedDescription string      `json:"detailed_description"`
		AboutTheGame        string      `json:"about_the_game"`
		ShortDescription    string      `json:"short_description"`
		SupportedLanguages  string      `json:"supported_languages"`
		HeaderImage         string      `json:"header_image"`
		Website             interface{} `json:"website"`
		PcRequirements      struct {
			Minimum string `json:"minimum"`
		} `json:"pc_requirements"`
		MacRequirements struct {
			Minimum string `json:"minimum"`
		} `json:"mac_requirements"`
		LinuxRequirements struct {
			Minimum string `json:"minimum"`
		} `json:"linux_requirements"`
		Developers    []string `json:"developers"`
		Publishers    []string `json:"publishers"`
		PriceOverview struct {
			FinalFormatted string `json:"final_formatted"`
		} `json:"price_overview"`
		Packages      []int `json:"packages"`
		PackageGroups []struct {
			Name                    string `json:"name"`
			Title                   string `json:"title"`
			Description             string `json:"description"`
			SelectionText           string `json:"selection_text"`
			SaveText                string `json:"save_text"`
			DisplayType             int    `json:"display_type"`
			IsRecurringSubscription string `json:"is_recurring_subscription"`
			Subs                    []struct {
				Packageid                int    `json:"packageid"`
				PercentSavingsText       string `json:"percent_savings_text"`
				PercentSavings           int    `json:"percent_savings"`
				OptionText               string `json:"option_text"`
				OptionDescription        string `json:"option_description"`
				CanGetFreeLicense        string `json:"can_get_free_license"`
				IsFreeLicense            bool   `json:"is_free_license"`
				PriceInCentsWithDiscount int    `json:"price_in_cents_with_discount"`
			} `json:"subs"`
		} `json:"package_groups"`
		Platforms struct {
			Windows bool `json:"windows"`
			Mac     bool `json:"mac"`
			Linux   bool `json:"linux"`
		} `json:"platforms"`
		Categories []struct {
			ID          int    `json:"id"`
			Description string `json:"description"`
		} `json:"categories"`
		Genres []struct {
			ID          string `json:"id"`
			Description string `json:"description"`
		} `json:"genres"`
		ReleaseDate struct {
			ComingSoon bool   `json:"coming_soon"`
			Date       string `json:"date"`
		} `json:"release_date"`
	} `json:"data"`
}

var (
	wLog     *log.Logger
	iLog     *log.Logger
	eLog     *log.Logger
	game     Game
	gamelist GameList
)

func main() {
	file, err := os.OpenFile("logs/logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	iLog = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	wLog = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	eLog = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)

	c := &http.Client{
		Timeout: 40 * time.Second,
	}

	URL, _ := url.Parse("https://store.steampowered.com/api/appdetails")
	appIDs := []int{20}
	g := make(map[int]Game)

	for _, appID := range appIDs {
		v := url.Values{}
		v.Set("appids", strconv.Itoa(appID))
		URL.RawQuery = v.Encode()
		req, err := http.NewRequest("GET", URL.String(), nil)
		if err != nil {
			eLog.Panicln(err)
		}
		resp, err := c.Do(req)
		if err != nil {
			eLog.Panicln(err)
		}
		defer resp.Body.Close()
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			eLog.Panicln(err)
		}
		if err := json.Unmarshal(body, &g); err != nil {
			eLog.Panicln(err)
		}
	}
	for _, d := range g {
		fmt.Printf("%+v", d.Data.Website)
	}
}
